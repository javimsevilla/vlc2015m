<?php

/**
 * Determines whether the given coordinates are the vertices
 * of a triangle.
 * 
 * @param array $p1 Point
 * @param array $p2 Point
 * @param array $p3 Point
 * @return boolan
 */
function esTriangulo($p1, $p2, $p3)
{
    return (($p2['y']-$p1['y'])/($p2['x']-$p1['x'])) != (($p3['y']-$p1['y'])/($p3['x']-$p1['x']));
}
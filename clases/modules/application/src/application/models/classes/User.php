<?php
namespace application\models\classes;

class User
{
    var $iduser;
    var $name;
    var $email;
    var $hobbies;
    
    function __construct($iduser, $name, $email, $hobbies)
    {
        $this->iduser = $iduser;
        $this->name = $name;
        $this->email = $email;
        $this->hobbies = $hobbies;
    }
}

?>
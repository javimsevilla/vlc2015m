<?php
namespace core\adapters

class core_adapters_MysqlAdapter
{
    private $config;
    private $link;
    
    
    public function __construct($config)
    {
        $this->config = $config;
        $this->connect();
    }
    
    function connect()
    {
        $this->link = mysqli_connect($this->config['host'],
                               $this->config['user'],
                               $this->config['password']
                              );
        mysqli_select_db($this->link, $this->config['database']);
    }
    
    public function queryUpdate($query)
    {
        return mysqli_query($this->link, $query);
    }
    
    public function querySelect($query)
    {
        $rows = array();
        $result = mysqli_query($this->link, $query);
        
        while($row = mysqli_fetch_assoc($result))
        {
            $rows[]=$row;
        }
        return $rows;
    }
    
    private function close()
    {
        mysqli_close($this->link);
    }
    
    public function __destruct()
    {
        $this->close();
    }
}
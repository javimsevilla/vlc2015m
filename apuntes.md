# Curso Desarrollo de Aplicaciones Web con PHP y MySQL sobre Apache #

## Info curso ##

*   **Contacto:** <servef_crnfp_paterna@gva.es>
*   **Google Plus:** CSF-CRNFP, Pensando en TIC
*   **Fecha inicio:** 20/ENE/2015
*   **Fecha fin:** 11/FEB/2015
*   **Docente:** Agustín Calderón (<agustincl@gmail.com>)
*   **Info máquina Windows del curso:**
    *   **Usuario:** `zendm`
    *   **Contraseña:** `zendm`

*****
**AL FINALIZAR EL CURSO SE ENTREGARÁ CERTIFICADO DE APROVECHAMIENTO DEL SERVEF Y DEL FABRICANTE**
*****

## Apuntes 20/ENE/2015 ##
### Instalación Entorno Desarrollo ###

*   [Zend-server](http://www.zend.com/en/products/server/downloads#Windows)
*   [Zend Studio](http://www.zend.com/en/products/studio)

### Cambiar DOCUMENT ROOT ###

*   Archivos del programa &rarr; Zend &rarr; Apache &rarr; conf &rarr; httpd.conf
*   Cambiar `DocumentRoot` y directiva `<Directory />`

### Marcas de cierre PHP ###

*   Nunca cerrarlas:
    
    ```php
    <?php
        // This is PHP
    ```

*   Son código PHP y podrían generarse cabeceras HTML imprevistas

### Siempre usar UTF-8 ###

*   Cambiar la configuración de Eclipse desde las preferencias para que use siempre codificación UTF-8

### Otra información de interés ###
>Alejandro Celaya ([GitHub](https://github.com/acelaya-blog),  [blog](http://blog.alejandrocelaya.com/))
>Marco Pivetta ([GitHub](https://github.com/ocramius))
>[365 Formas de buscar trabajo blog](http://365formasdepedirtrabajo.com/) &rarr; Blog sobre buscar empleo

## Apuntes 21/ENE/2015 ##


### `echo` vs `print` ###

**echo** es una instrucción del sistema PHP mientras que **print** es una función por tanto la forma correcta de usarlas es:

```php
print("");
echo "";
```

### Como documentar PHP ###

*   Mediante [PHPDocumentor](http://www.phpdoc.org/).
*   Siempre escribir toda la documentación, nombres de ficheros y variables en inglés.

### Estructurar el código ###

Los ficheros **deben estar separados** en una estructura organizada según algún patrón de diseño. No deberían estar todos en la raíz del proyecto.

*****

Los valores `TRUE` y `FALSE` siempre deben ir en mayúsculas.

*****

Mediante los corchetes `{<expression>}` se pueden evaluar expresiones.

*****

Las constantes siempre se escriben en mayúsculas.

*****

Las constantes no llevan el símbolo `$` delante.

*****

El operador identidad `===` se evalua a `TRUE` si ambos operandos coinciden en **tipo** y **valor**.

*****

El operador `@` oculta los errores mostrados en la salida.

*****

## Nomenclatura ##

*   / &rarr; *slash*
*   \ &rarr; *backslash*
*   & &rarr; *ampersand*
*   # &rarr; *sharp*
*   | &rarr; *pipe*

*****

Con el operador `xor` no se conoce de dónde viene el valor verdadero (lo cual es idóneo para encriptación de un solo sentido).

*****

`&&` y `||` &rarr; Tienen más preferencia que `and` y `or` con letra.

*****

`4 << 2` &rarr; Es un desplazamiento a la izquierda a nivel binario. Es decir 4 en binario es 100 que desplazado 2 posiciones es 10000 (en decimal 16).

`4 >> 2` &rarr; Es un desplazamiento a la derecha a nivel binario.

*****

**Ejercicio:** Genera dinámicamente la siguiente tabla.

<table border="1">
    <tr>
        <td>0</td>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
    </tr>
    <tr>
        <td>1</td>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
    </tr>
    <tr>
        <td>2</td>
        <td>2</td>
        <td>4</td>
        <td>6</td>
        <td>8</td>
        <td>10</td>
    </tr>
    <tr>
        <td>3</td>
        <td>3</td>
        <td>6</td>
        <td>9</td>
        <td>12</td>
        <td>15</td>
    </tr>
    <tr>
        <td>4</td>
        <td>4</td>
        <td>8</td>
        <td>12</td>
        <td>16</td>
        <td>20</td>
    </tr>
</table>

*****

**Ejercicio:** Genera dinámicamente la sucesión de fibonacci

0, 1, 1, 2, 3, 5, 8, 13, 21, ...

*****

**Ejercicio:** Desarrollar una función que dados 3 puntos indique si forman un triángulo o no.
>3 puntos forman un triángulo si dos de ellos forman una recta y el tercero no pertenece a dicha recta.

*****

**Ejercicio:** Calcular la ecuación de la curva dados los puntos A y B

![Función seno](images/sinus.gif)

### Funciones ###

![Ámbito de funciones](images/functionScope.png)

Valores por defecto siempre al final

```php
function nombreFuncion($nom, $titulo = "Sr.") {
    // ...
}
```

El paso por referencia está permitido:

```php
llamadaAunaFuncion(&$variable); // Paso por referencia
```

*****

**Ejercicio:** Integrar funciones

![Integrar funciones](images/integracion.png)

**¡IMPORNTANTE!** UNA FUNCIÓN POR FICHERO (BUENA PRÁCTICA). UN FICHERO DE PRUEBAS POR CADA FUNCIÓN. P.ej.: `testFibonacci.php`. Jamás sacar nada por pantalla en ficheros de funciones.

*****

### Tipos de notación por convenio ###

*   **UPPERCASE** &rarr; Constantes
*   **camelCase** &rarr; Funciones y nombres de ficheros
*   **snake_case** &rarr; Para variables
*   **lowercase** &rarr; Todo lo demás

*****

**TDD** &rarr; Test Driven Development (Test unitarios)

## Apuntes 22/ENE/2015 ##
## Apuntes 23/ENE/2015 ##

## Apuntes 26/ENE/2015 ##

### Modelo, Vista, Controlador ###

Patrón de diseño (años 60).

La Vista jamás debe conectar con el Modelo.

El Modelo son las acciones (verbos).

Los recursos (usuarios) es un nombre (users).

Métodos protocolo HTTP:

GET, POST, PUT/UPDATE, DELETE, OPTIONS, TRACE, CONNECT

/usuarios/index
/usuarios/update/id/8
/controllers/action/param1/val1/param1/val2

### Patrón de diseño Bootstrap ###

**NO ES EL FRAMEWORK** es un patrón de diseño.

index.php
.htaccess

Atrapa todas las URL y las envía al index para que las procese.


## Apuntes 27/ENE/2015 ##
## Apuntes 28/ENE/2015 ##

### Bases de datos con MySQL ###

Instalación mysql workbench.

¿utf8\_general\_ci?

general = ordenamiento ANSI de los carácteres (orden alfabético para cada idioma es diferente)
ci = case insensitive

spanish2 es español de sudamerica donde la 'ch' va después de 'c'

P.ej.:

Depende el orden estas palabras se ordenan de forma diferente:

cuenca
chile
ceuta

Los datos personales de usuario no pueden usarse como claves por la LOPD.
Se pueden pedir al usuario, pero no usuarlos como claves.

Tampoco deben usarse ID autoincrementales porque pueden adivinarse.
NUNCA USAR AUTOINCREMENTAL CON DATOS SENSIBLES.

Buscar UUID y GUID (Universal Unique Idintifier)

Las fotos no se guardan en la base de datos, se guardan las rutas.

Si se necesita guardar un archivo binario se guarda en un campo de tipo BLOB (binario).

File > New model
Despues de crear el modelo:
Database > Synchronize model

Buscar SCRUM (metodologías de software Agile/Xtreme programming)

sprint?

Xtreme programming: programar a cuatro manos.

1 controla el teclado
1 controla el ratón (éste manda)

Después de crear la base de datos (deploy) hay que rellenar las tablas paramétricas (las que estan en rojo)

Son los ALTER TABLE (SQL Codigo) lo que hay que guardar cuando se cambia el diseño.

Expresiones regulares MySQL

\* = %

## Apuntes 29/ENE/2015 ##

Instalación de Bower mediante NodeJS

## Apuntes 30/ENE/2015 ##

1.- Borrar bower_components
2.- Crear /www/cruddb/public/.bowerrc con este contenido:

{
    "directory":"assets/vendor"
}

3.- Volver a ejecutar "$ boweb install bootstrap"
4.- Copiar del crudtxt las carpetas a cruddb

config
data
modules
.htaccess

5.- Corregir rutas de los layouts "jumbotron.phtml" y "dashboard.phtml".

## Apuntes 02/FEB/2015 ##

Examen certificación: (la semana después de semana santa)

Traer 2 identificaciones como mínimo (DNI, carnet de conducir).
Para aprobar el examen hay que acertar el 70% de las preguntas.
Validación internacional.

2525€

http://www.php-fig.org

API CENTRY
SOAP
API RestFull
API cuesta 8000€

mysql functions (no transaccional)
mysqli functions (permite transacciones)

describe users; <---- Devuelve la estructura de la tabla

## Apuntes 03/FEB/2015 ##

self::myStaticFuntion()
object->publiMethod()
"::" &larr; Es un operador de resolución de ámbito (paamayim nekudotayim)
"->" &larr; Es el operador

Métodos mágicos (para construir y destruir objetos)

__construct()
__destruct()

VERSIONADO DE APLICACIONES http://semver.org/

idratador?
coleccion de entidades idratadas?

## Apuntes 04/FEB/2015 ##

Adaptadores: cerrar la conexión siempre en el __destruct()
get_class_methods($class_name) &rarr; indica los metodos que tiene una clase disponibles

## Apuntes 05/FEB/2015 ##

Singleton (patron de diseño) va a ser el FrontController

La separación de elementos (desacoplamiento) entre front controller y controladores se llama inyección de dependencias (dependency injection (DI))

Se debería hacer mediante la herencia y las implementación de interfaces si es obligatoria. Si la dependencia es opcional se pasa por getters y setters (pasando por parametros los valores de una clase a otra)

A las funciones se les puede decir el tipo de parametro que se le está pasando como en otros lenguajes fuertemente tipados como Java o C.

También se pueden esperar variables de tipo objeto y que además cumplan una interfaz de la siguiente forma

function miFuncion(ObjetoInterface variable1) {...} // variable1 debe ser un objeto y además debe implementar la interfaz "ObjetoInterface"

con __LINE__ se puede hacer un hash de varias líneas y varios ficheros etc para comprobar que no se modifica un codigo fuente y por tanto evitar modificaciones e incumplimiento de licencias.

## Apuntes 06/FEB/2015 ##

Mapper (patron de diseño)

http://codeception.com/ (Para Test Driven Development)

los "namespaces" equivalen a los "package" de Java
Los "use <namespace>" equivalen a los "import" de Java

"postman" chrome app para probar peticiones via POST/GET/UPDATE/... (API REST)

## Apuntes 09/FEB/2015 ##
## Apuntes 10/FEB/2015 ##

Dar salida en red local al servidor apache (un proyecto concreto)

  copiar directiva VirtualBox del proyecto. Cambiar puerto 80 al 81 y añadir justo bajo la linea de `NameVirtualHost *:80` la linea `Listen 81`. De esa forma poniendo la IP de la maquina donde esté el proyecto (y el server) y el puerto <ip>:81/ será la raiz del proyecto. ¡Importante! Hay que quitar el ServerName de la directiva VirtualHost

  Aplicacion de chrome para probar API RESTFull: postman

  HAL - Hypertext Application Language
  apigility.org
  framework.zend.com

## Apuntes 11/FEB/2015 ##

Mirar el ChangeLog de PHP 5

Evitar ataques csrf (cross site .. form) añadiendo un campo oculto al formulario (un token de firma) que identifique al formulario.
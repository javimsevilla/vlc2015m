-- MySQL Workbench Synchronization
-- Generated: 2015-01-28 11:24
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: zendm

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `crud` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS `crud`.`users` (
  `iduser` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `photo` VARCHAR(255) NULL DEFAULT NULL,
  `bdate` DATETIME NULL DEFAULT NULL,
  `cities_idcity` INT(11) NOT NULL,
  `genders_idgender` INT(11) NOT NULL,
  PRIMARY KEY (`iduser`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_users_cities_idx` (`cities_idcity` ASC),
  INDEX `fk_users_genders1_idx` (`genders_idgender` ASC),
  CONSTRAINT `fk_users_cities`
    FOREIGN KEY (`cities_idcity`)
    REFERENCES `crud`.`cities` (`idcity`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_genders1`
    FOREIGN KEY (`genders_idgender`)
    REFERENCES `crud`.`genders` (`idgender`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crud`.`cities` (
  `idcity` INT(11) NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`idcity`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crud`.`hobbies` (
  `idhobby` INT(11) NOT NULL AUTO_INCREMENT,
  `hobby` VARCHAR(255) NULL DEFAULT NULL,
  `hobbiescol` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idhobby`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crud`.`genders` (
  `idgender` INT(11) NOT NULL,
  `gender` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idgender`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crud`.`agreetments` (
  `idagreetment` INT(11) NOT NULL,
  `agreetment` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`idagreetment`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crud`.`users_has_hobbies` (
  `users_iduser` VARCHAR(255) NOT NULL,
  `hobbies_idhobby` INT(11) NOT NULL,
  PRIMARY KEY (`users_iduser`, `hobbies_idhobby`),
  INDEX `fk_users_has_hobbies_hobbies1_idx` (`hobbies_idhobby` ASC),
  INDEX `fk_users_has_hobbies_users1_idx` (`users_iduser` ASC),
  CONSTRAINT `fk_users_has_hobbies_users1`
    FOREIGN KEY (`users_iduser`)
    REFERENCES `crud`.`users` (`iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_hobbies_hobbies1`
    FOREIGN KEY (`hobbies_idhobby`)
    REFERENCES `crud`.`hobbies` (`idhobby`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

CREATE TABLE IF NOT EXISTS `crud`.`users_has_agreetments` (
  `users_iduser` VARCHAR(255) NOT NULL,
  `agreetments_idagreetment` INT(11) NOT NULL,
  PRIMARY KEY (`users_iduser`, `agreetments_idagreetment`),
  INDEX `fk_users_has_agreetments_agreetments1_idx` (`agreetments_idagreetment` ASC),
  INDEX `fk_users_has_agreetments_users1_idx` (`users_iduser` ASC),
  CONSTRAINT `fk_users_has_agreetments_users1`
    FOREIGN KEY (`users_iduser`)
    REFERENCES `crud`.`users` (`iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_agreetments_agreetments1`
    FOREIGN KEY (`agreetments_idagreetment`)
    REFERENCES `crud`.`agreetments` (`idagreetment`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

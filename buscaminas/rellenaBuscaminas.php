<?php

function rellenaBuscaminas($filas, $columnas)
{
    $minas = ($filas * $columnas) / 5;
    $canvas = array();
    $i = $minas;
    
    while( $i > 0 )
    {
        for( $j = 0; $j < $filas; $j++ )
        {
            for( $k = 0; $k < $columnas; $k++ )
            {
                if(!isset($canvas[$j][$k]))
                {
                    if($i > 0)
                    {
                        if( rand(0,10) > 9 )
                        {
                            $canvas[$j][$k] = '*';
                            $i--;
                        }
                    }
                }
            }
        }
    }

    for( $i = 0; $i < $filas; $i++ )
    {
        for( $j = 0; $j < $columnas; $j++ )
        {
            if(!isset($canvas[$i][$j]))
            {
                $canvas[$i][$j] = 0;
            }
        }
    }

    for( $i = 1; $i < $filas; $i++ )
    {
        for( $j = 1; $j < $columnas; $j++ )
        {
            if( $canvas[$i][$j] == '*' )
            {
                $arriba = &$canvas[$i+1][$j];
                $arriba_derecha = &$canvas[$i+1][$j+1];
                $derecha = &$canvas[$i][$j+1];
                $abajo_derecha = &$canvas[$i-1][$j+1];
                $abajo = &$canvas[$i-1][$j];
                $abajo_izquierda = &$canvas[$i-1][$j-1];
                $izquierda = &$canvas[$i][$j-1];
                $arriba_izquierda = &$canvas[$i+1][$j-1];

                if(isset($arriba) && $arriba != '*')                       $arriba++;
                if(isset($arriba_derecha) && $arriba_derecha != '*')       $arriba_derecha++;
                if(isset($derecha) && $derecha != '*')                     $derecha++;
                if(isset($abajo_derecha) && $abajo_derecha != '*')         $abajo_derecha++;
                if(isset($abajo) && $abajo != '*')                         $abajo++;
                if(isset($abajo_izquierda) && $abajo_izquierda != '*')     $abajo_izquierda++;
                if(isset($izquierda) && $izquierda != '*')                 $izquierda++;
                if(isset($arriba_izquierda) && $arriba_izquierda != '*')   $arriba_izquierda++;
            }
        }
    }

    return $canvas;
}
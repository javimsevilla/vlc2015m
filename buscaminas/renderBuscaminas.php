<?php

function renderBuscaminas($canvas)
{
    $html = "<table border=1>";

    for( $i = 0; $i < sizeof($canvas); $i++ )
    {
        $html .= "<tr>";
        for( $j = 0; $j < sizeof($canvas[$i]); $j++ )
        {
            $html .= "<td style=\"width: 30px; height: 30px; text-align: center;\">";
            if( $canvas[$i][$j] > 0 || $canvas[$i][$j] == '*')
            {
                $html .= $canvas[$i][$j];
            }
            $html .= "</td>";
        }
        $html .= "</tr>";
    }

    $html .= "</table>";

    return $html;
}